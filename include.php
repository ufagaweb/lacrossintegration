<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses('Lacross.triolan', array(
    'Lacross\SOAP\SOAPClient' => 'lib/SOAPClient.php',
));