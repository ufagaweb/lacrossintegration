<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

if (class_exists('lacross_triolan')) {
    return;
}
class maycat_d7dull extends CModule
{
    /** @var string */
    public $MODULE_ID;
    /** @var string */
    public $MODULE_VERSION;
    /** @var string */
    public $MODULE_VERSION_DATE;
    /** @var string */
    public $MODULE_NAME;
    /** @var string */
    public $MODULE_DESCRIPTION;
    /** @var string */
    public $MODULE_GROUP_RIGHTS;
    /** @var string */
    public $PARTNER_NAME;
    /** @var string */
    public $PARTNER_URI;

    public function __construct()
    {
        $this->MODULE_ID = 'lacross_triolan';
        $this->MODULE_VERSION = '0.0.1';
        $this->MODULE_VERSION_DATE = '2018-01-12 12:00:00';
        $this->MODULE_NAME = 'Lacross integration with Triolan';
        $this->MODULE_DESCRIPTION = 'Description';
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME = "Lacross";
        $this->PARTNER_URI = "http://www.lacross.ru";
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
    }

    public function doUninstall()
    {
        ModuleManager::unregisterModule($this->MODULE_ID);
    }
}