<?php
namespace Lacross\SOAP;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

class SOAPClient {
    private $SOAP_LOGIN;
    private $SOAP_PASSWORD;
    private $TREOLAN_WSDL = 'https://api.treolan.ru/webservices/treolan.wsdl';

    public function __construct($login, $password) {
        $this->SOAP_LOGIN = $login;
        $this->SOAP_PASSWORD = $password;
    }

    private function request($methodName, $params = []) {
        $soapClient = new \SoapClient($this->TREOLAN_WSDL, ['connection_timeout' => 300]);

        $params['login'] = $this->SOAP_LOGIN;
        $params['password'] = $this->SOAP_PASSWORD;

        try {
            $data = $soapClient->__call($methodName, $params);
            return $data['Result'];
        } catch (\SoapFault $fault) {
            return new \ErrorException($fault->getMessage());
        }
    }

    public function call($methodName, $params = []) {
        return $this->request($methodName, $params);
    }
}