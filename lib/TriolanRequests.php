<?php
namespace Lacross\SOAP;

use Lacross\SOAP\SOAPClient;

class TriolanRequests {

    private $CLIENT;

    public function __construct($login, $password) {
        $this->CLIENT = new SOAPClient($login, $password);
    }

    public function getProductInfo($articul) {
        return $this->CLIENT->call('ProductInfoV2', ['Articul' => $articul]);
    }

    public function createOrder($payerID, $positions) {
        $params = [
            'orderXml' => $this->buildXMLOrderQuery($payerID, $positions),
            'useCode' => 0,
        ];

        return $this->CLIENT->call('NewOrder', $params);
    }

    private function buildXMLOrderQuery($payerID, $positions = [], $comment = '') {
        $element = '<o:root xmlns:o="urn:order">';
        $element .= '<order payer_id="' . $payerID . '" type="D" stock="1" comment="' . $comment . '">';
        foreach ($positions as $position) {
            $element .= '<position articul="' . $position['articul'] . '" quantity="' . $position['quantity'] . '" />';
        }
        $element .= '</order></o:root>';

        return $element;
    }

}